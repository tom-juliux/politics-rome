using System.Collections;
using UnityEngine;

public class InkDialogueReference : IInteractable
{
    public string reference;
    public string speakerName;
    public override void Interact()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().pause = true;
        FindObjectOfType<inkDialogueManager>().StartDialogue(reference, speakerName);
    }

}
