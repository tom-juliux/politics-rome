using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Collider))]
public abstract class IInteractable : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
            Interact();
    }
    public abstract void Interact();
}