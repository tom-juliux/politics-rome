﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    public float speed = 1;
    public bool pause = false;
    CharacterController controller;

    void Start()
    {
        controller = GetComponent<CharacterController>();

    }
    void Update()
    {
        if (pause)
            return;
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        controller.Move(new Vector3(x, 0, z));
        //transform.position += new Vector3(x, 1, z);

    }
}
