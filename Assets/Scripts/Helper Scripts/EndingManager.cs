using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class EndingManager : MonoBehaviour
{
    public Text UIText;
    public GameObject solidar;
    void Start()
    {
        Statistic s = GameObject.FindGameObjectWithTag("Statistic").GetComponent<Statistic>();
        StateStates Endstate = s.EndState;
        if (Endstate == StateStates.CONSTITUTIONALEND || Endstate == StateStates.ELECTEDOUTOFFOFFICE || Endstate == StateStates.LEADERDIEDOFAGE)
        {
            GetComponent<Animator>().SetTrigger("Disappear");
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            solidar.SetActive(true);
            GetComponent<Animator>().SetBool("Talking", true);
            transform.position = new Vector3(0, 1.1f, 0);
            transform.rotation = Quaternion.Euler(-90f, 0, 0);
            ParticleSystem p = transform.GetChild(0).GetComponent<ParticleSystem>();
            p.startColor = Color.HSVToRGB(0, 197, 255);
            p.transform.rotation = Quaternion.Euler(270, 0, 0);
            p.startSpeed = 5;
            p.gravityModifier = 1;
            transform.GetChild(0).gameObject.SetActive(true);
        }
        string endText;
        switch (Endstate)
        {
            case StateStates.CONSTITUTIONALEND:
                endText = "After " + s.TotalTime + " month the ruler gave returned his power to the consttitution and rest with the great statesman in the sky.";
                break;
            case StateStates.LEADERDIEDOFAGE:
                endText = "After " + s.TotalTime + " month the ruler died and joined the great statesman in the sky.";
                break;
            case StateStates.ELECTEDOUTOFFOFFICE:
                endText = "After " + s.TotalTime + " month the ruler was elected out of office and later joined the great statesman in the sky.";
                break;
            case StateStates.DESTROYED:
                endText = "After " + s.TotalTime + " month in office, Rome was defeated. In the last minute before the city felt it´s ruler was executed.";
                break;
            case StateStates.BROKE:
                endText = "After " + s.TotalTime + " month the ruler mismanaged the finaces of the state and was executed by the public for the hyperinflated price of 1 Trillion Sesterz. (About 1.2US$)";
                break;
            case StateStates.REVOLUTION:
                endText = "After " + s.TotalTime + " month the ruler was executed by an angry crowd.";
                break;
            default:
                endText = "";
                break;
        }
        UIText.text = endText;
    }

    public void remove()
    {
        SceneManager.LoadScene(0);
    }
}