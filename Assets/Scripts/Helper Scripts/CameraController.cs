using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes the camera follow the player

public class CameraController : MonoBehaviour
{

    public Transform target;    // Target to follow (player)
    public float smooth;
    public float minX;
    public float maxX;
    public Vector3 offset;          // Offset from the player
    void Start()
    {
    }
    void Update()
    {
    }

    void LateUpdate()
    {
        if(!target.gameObject.activeSelf)
            return;
        Vector3 desiredPosition = new Vector3(Mathf.Clamp(target.position.x, minX, maxX), offset.y, offset.z);// target.position + offset;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, smooth * Time.deltaTime);
        transform.LookAt(target.position);
    }

}