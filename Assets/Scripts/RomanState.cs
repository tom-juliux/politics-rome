using System.Collections.Generic;
using UnityEngine;


public class RomanState : State
{

    public RomanState(Tendenzies tendenzies, float scale, int money) : base(tendenzies, scale, money)
    {
    }
    override
    public StateStates HandleElection(int TimeInOffice)
    {
        difficulty.popularityMonthly = -Mathf.Clamp(Mathf.FloorToInt(Mathf.Abs((popularity + TimeInOffice) * 0.2f)),0,popularity);

        if (money < 0)
            if (atWar)
                return StateStates.DESTROYED;
            else
                return StateStates.BROKE;
        if (TimeInOffice > 20 * 6)
            return StateStates.LEADERDIEDOFAGE;
        if (TimeInOffice >= 4 * 6 && !atWar)
            return StateStates.CONSTITUTIONALEND;
        if (popularity < -10)
            return StateStates.REVOLUTION;
        if (agreement < 0.2)
            return StateStates.ELECTEDOUTOFFOFFICE;
        return StateStates.RUNNING;
    }
}