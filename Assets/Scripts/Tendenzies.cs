using UnityEngine;
[System.Serializable]
public struct Tendenzies : System.IEquatable<Tendenzies>
{
    //Variable declaration
    //Note: I'm explicitly declaring them as public, but they are public by default. You can use private if you choose.
    public float privacy;
    public float war;
    public static Tendenzies Empty
    {
        get
        {
            return new Tendenzies(0, 0, 0, 0);
        }
    }
    public float individualism;
    public float strongLeader;
    public Tendenzies(float privacy, float war, float individualism, float strongLeader)
    {
        this.privacy = privacy;
        this.war = war;
        this.individualism = individualism;
        this.strongLeader = strongLeader;
    }
    public bool Equals(Tendenzies other)
    {
        return privacy == other.privacy &&
        this.war == other.war &&
        this.individualism == other.individualism &&
        this.strongLeader == other.strongLeader;

    }
    public static Tendenzies operator +(Tendenzies a, Tendenzies b)
    {
        return new Tendenzies(Mathf.Clamp(a.privacy + b.privacy, 0, 1), Mathf.Clamp(a.war + b.war, 0, 1),
        Mathf.Clamp(a.individualism + b.individualism, 0, 1), Mathf.Clamp(a.strongLeader + b.strongLeader, 0, 1));
    }
    public static float Compare(Tendenzies a, Tendenzies b)
    {
        return (Mathf.Abs(a.privacy - b.privacy) +
        Mathf.Abs(a.war - b.war) +
        Mathf.Abs(a.individualism - b.individualism) +
        Mathf.Abs(a.strongLeader - b.strongLeader))
        / 4;
    }
    public static float CompareAbs(Tendenzies a, Tendenzies b)
    {
        return (((a.privacy * b.privacy > 0) ? 1 : -1) +
                ((a.war * b.war > 0) ? 1 : -1) +
                ((a.individualism * b.individualism > 0) ? 1 : -1) +
                ((a.strongLeader * b.strongLeader > 0) ? 1 : -1));
    }
}