using UnityEngine;
public class PerkToggle : MonoBehaviour
{
    [SerializeField]
    public Perk perk;
    public void toggle(bool value)
    {
        GameObject.FindGameObjectWithTag("StateManager").GetComponent<StateManager>().togglePerk(perk);
    }
    public void showDescription()
    {
        GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>().setDiscription(perk);
    }
}