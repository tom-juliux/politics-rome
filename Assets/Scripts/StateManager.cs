using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ink;
public enum Topics { Privacy, War, Individualism, StrongLeader };

public class StateManager : MonoBehaviour
{
    public State state;
    public UIManager ui;
    public Tendenzies tendenzies;

    [SerializeField]
    public float secoundsToMonths = 20f;
    public bool paused = false;
    public GameObject StatisticsPrefab;
    public int money = 100;
    private float currentTime = 0;
    private int totalTime;
    void Start()
    {
        state = new RomanState(tendenzies, 40f, money);
        updateUI();
        StartCoroutine("countdown");
    }

    IEnumerator countdown()
    {
        for (;;)
        {

            if (paused)
            {
                yield return new WaitForSeconds(0.25f);
                continue;
            }
            currentTime += 0.25f;
            ui.setCountdownSlider(currentTime / secoundsToMonths);
            updateUI();
            if (currentTime < secoundsToMonths)
            {
                yield return new WaitForSeconds(0.25f);
                continue;
            }
            currentTime = 0f;
            totalTime++;
            ui.setTIO(totalTime);
            state.HandlePerks();
            yield return new WaitForSeconds(0.25f);
        }
    }
    void updateUI()
    {
        ui.setAgreementText(state.calcAgreement());
        ui.setSupportText(state.calcSupport());
        ui.setDonationsText(state.calcDonations());
        ui.setExpensesText(state.calcExpenses());
        ui.setPopularirtyText(state.popularity);
        ui.setMoneyText(state.money);
        ui.setTendenzyTexy(state.calcTedenzie());
    }
    void Update()
    {
        StateStates zustand = state.HandleElection(totalTime);
        if (zustand == StateStates.RUNNING) return;
        else GameOver(zustand);
    }
    public void togglePerk(Perk perk)
    {
        if (!state.perks.Contains(perk))
            state.AddPerk(perk);
        else
            state.RemovePerk(perk);
    }
    void GameOver(StateStates reason)
    {
        Debug.Log("GameEndend because of " + reason);
        StopCoroutine("countdown");
        Statistic s = Instantiate(StatisticsPrefab, Vector3.zero, Quaternion.identity).GetComponent<Statistic>();
        s.EndState = reason;
        s.TotalTime = totalTime;
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

}